# LSED

Repozytorim na Laboratorium Statystycznej Eksploracji Danych


Strona laboratorium: http://www.if.pw.edu.pl/~julas/LSED/zad.html

mini-projekt problem (klasyczny): http://if.pw.edu.pl/~agatka/labkomp/LG2.pdf

Strona z danymi do projektu: https://github.com/awesomedata/awesome-public-datasets

## Laboratorium 0 (05.03.2020)
### zadanie 0
Dyskryminacja Fisherowska (rzutowanie klas)
    
## Laboratorium 1 (12.03.2020)
### zadanie 1
Naiwny klasyfikator Bayesa

## Laboratorium 2 (19.03.2020)
### zadanie 2
Klasyfikacja zbioru win (naiwny Bayes, LDA, QDA, kroswalidacja)
    
## Laboratorium 3 (26.03.2020)
### zadanie 3
Badanie skuteczności klasyfikatora k_NN
    
## Laboratorium 4 (02.04.2020)
### zadanie 4
Drzewa decyzyjne

## Laboratorium 5 (16.04.2020)
### zadanie 5
Klasyfikacja bagging

## Laboratorium 6 (23.04.2020)
### zadanie 6
Klasyfikator SVM

## Laboratorium 7 (30.04.2020)
### zadanie 7
funkcje heatmap oraz kmeans

## Laboratorium 8 (07.05.202)
### zadanie 8
analiza PCA

